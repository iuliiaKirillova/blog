import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../modules/auth/services/auth.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  email: string = null;
  isAuth: boolean = false;

  constructor(
    private readonly authService: AuthService
  ) { }

  ngOnInit(): void {
    this.isAuth = this.authService.isAuthunticated();
    this.email = this.authService.getEmail();
  }

  logOut(): void {
    return this.authService.logout();
  }

}
