import {Component, OnInit} from '@angular/core';
import {PostsService} from "../../../../services/posts.service";
import {Post} from "../../../../interfaces/post";
import {Router} from "@angular/router";

@Component({
  selector: 'app-posts-component',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {

  posts: Post[] = []

  constructor(
    private readonly postsService: PostsService,
    // private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.postsService.find().subscribe((posts: Post[]) => this.posts = posts);

  }

  // go(id: string) {
  //   // this.router.navigateByUrl('/posts/' + id),
  //   //   this.router.navigate([id]);
  // }

}
