import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {PostsService} from "../../../../services/posts.service";
import {first, map, switchMap, tap} from "rxjs/operators";
import {Post} from "../../../../interfaces/post";

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  id: string = '';
  post: Post = null;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private postsService: PostsService
  ) { }

  ngOnInit(): void {

    this.loadPost();
  }

  private loadPost(): void {
    this.route.params.pipe(
      // first(),
      switchMap(({ id }: Params) => this.postsService.findOne(id).pipe(
        tap((post: Post) => this.post = post)
      ))
    ).subscribe();
  }

}
