import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {PostsService} from "../../../../services/posts.service";
import {Post} from "../../../../interfaces/post";
import {switchMap, tap} from "rxjs/operators";

@Component({
  selector: 'app-post-update',
  templateUrl: './post-update.component.html',
  styleUrls: ['./post-update.component.scss']
})
export class PostUpdateComponent implements OnInit {

  form: FormGroup = new FormGroup({
    title: new FormControl(null, Validators.required),
    author: new FormControl(null, Validators.required),
    content: new FormControl(null, Validators.required),
  })

  constructor(
    private readonly router: Router,
    private readonly postsService: PostsService,
    private route: ActivatedRoute
  ) { }

  id: string = null;

  private loadPost(): void {
    this.route.params.pipe(
      switchMap((params: Params) => this.postsService.findOne(params.id).pipe(
      tap((post: Post) => this.form.patchValue(post))
  ))).subscribe();
      this.route.params.subscribe(
        (params: Params) => this.id = params.id
      )
  }

  ngOnInit(): void {
    this.loadPost()
  }

  submit(): void {
    const post: Post = this.form.value;
    this.postsService.update(this.id, post).subscribe(
      () => this.router.navigateByUrl('/admin/posts')
    );
  }

}
