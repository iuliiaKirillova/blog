import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {PostsService} from "../../../../services/posts.service";
import {Post} from "../../../../interfaces/post";
import {Router} from "@angular/router";

@Component({
  selector: 'app-post-form',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.scss']
})
export class PostCreateComponent implements OnInit {

  form: FormGroup = new FormGroup({
    title: new FormControl(null, Validators.required),
    author: new FormControl(null, Validators.required),
    content: new FormControl(null, Validators.required),
  })

  constructor(
    private readonly router: Router,
    private readonly postsService: PostsService
  ) { }

  ngOnInit(): void {
  }

  submit(): void {
    const post: Post = this.form.value;
    this.postsService.create(post).subscribe(
      () => this.router.navigateByUrl('/admin/posts')
    );
  }

}
