import { Component, OnInit } from '@angular/core';
import {Post} from "../../../../interfaces/post";
import {PostsService} from "../../../../services/posts.service";

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {

  posts: Post[] = [];

  displayedColumns: string[] = ['position', 'title', 'author', 'action'];

  constructor(
    private readonly postsService: PostsService
  ) { }

  ngOnInit(): void {
    this.postsService.find().subscribe(
      (posts: Post[]) => this.posts = posts);
  }

  remove(id:string): void {
      this.posts = this.posts.filter(
        (post: Post) => post.id !== id
      );
    this.postsService.delete(id).subscribe();
  }
}
