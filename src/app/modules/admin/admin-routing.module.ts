import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PostCreateComponent} from "./components/post-create/post-create.component";
import {PostComponent} from "../blog/components/post/post.component";
import {AuthGuard} from "../auth/guards/auth.guard";
import {PostsComponent} from "./components/posts/posts.component";
import {PostUpdateComponent} from "./components/post-update/post-update.component";

const routes: Routes = [
  { path: 'posts', component: PostsComponent, canActivate: [AuthGuard] },
  { path: 'posts/create', component: PostCreateComponent, canActivate: [AuthGuard] },
  { path: 'posts/:id', component: PostComponent, canActivate: [AuthGuard] },
  { path: 'posts/:id/update', component: PostUpdateComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
