import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import {environment} from "../../../../environments/environment";
import {tap} from "rxjs/operators";
import {FBAuthResponse} from "../interfaces/auth.service";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  get token(): string {
    const dateExp: string = localStorage.getItem('DATE_EXP');

    if (!dateExp) {
      return null;
    }

    if (new Date() > new Date(dateExp)) {
      this.logout();
      return null;
    }
    return localStorage.getItem('ACCESS_TOKEN');
  }

  constructor(
    private readonly router: Router,
    private readonly httpClient: HttpClient
  ) { }

  getEmail(): string {
    return localStorage.getItem('EMAIL');
  }

  isAuthunticated(): boolean {
    return !!this.token;
  }

  login(email: string, password: string) {
console.log(email, password);
    return this.httpClient.post<FBAuthResponse>(
      `${environment.authUrl}/accounts:signInWithPassword?key=${environment.apiKey}`,
      { email, password, returnSecureToken: true },
      { headers: {
        'Content-Type': 'application/json'
        }}
    ).pipe(
      tap((res: FBAuthResponse) => {
        const { idToken, expiresIn, email } = res;
        if (!idToken || !expiresIn) {
          return
        }

        const expDate: Date = new Date(new Date().getTime() + +expiresIn * 1000);

        localStorage.setItem('ACCESS_TOKEN', idToken);
        localStorage.setItem('DATE_EXP', expDate.toString());
        localStorage.setItem('EMAIL', email);
      })
    )
  }


  logout(): void {
    localStorage.clear();
    this.router.navigateByUrl('/auth/sign-in')
  }

}
